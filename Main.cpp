#include <fstream>
#include <stdio.h>
#include <string.h>
#include <math.h>

#define WIDTH 100
#define HEIGHT 200

int x;
char* path;

std::ofstream fout;

char** frame;

struct VERTEX
{
	int x;
	int y;
};

struct RECT
{
	int x;
	int y;
	int width;
	int height;
};

VERTEX CalcTreePosition(int, int);

VERTEX* goodForGlobes;
int indexGood = 0;

void ParseArguments(int argc, char* argv[])
{
	int path_lung = strlen(argv[2]);

	sscanf(argv[1], "%d", &x);

	path = new char[path_lung];

	memset(path, NULL, sizeof(char) * path_lung);

	sscanf(argv[2], "%s", path);
}

void CreateOutputStream()
{
	fout.open(path);
}

void InitFrame(int width,int height)
{
	frame = new char*[height];

	for (int i = 0;i < height;i++)
	{
		frame[i] = new char[width];
		memset(frame[i], '*', sizeof(char)*width);
	}

	for (int i = 0;i < width;i++)
	{
		int beg = CalcTreePosition(width, height).y - 15;
		if (i % 2 == 0)
		{
			frame[beg][i] = '^';
		}
		else
		{
			frame[beg][i] = '~';
		}

		for (int j = beg + 1;j < height;j++)
		{
			frame[j][i] = '`';
		}
	}

	goodForGlobes = new VERTEX[WIDTH*HEIGHT];
}

void WriteOutputFrame(int width, int height)
{
	for (int i = 0;i < height;i++)
	{
		for (int j = 0;j < width;j++)
		{
			fout << frame[i][j];
		}
		fout << "\n";
	}
}

VERTEX CalcTreePosition(int width,int height)
{
	VERTEX result;

	result.x = width / 2;
	result.y = (height - (height/10));

	return result;
}

void DrawPattern(VERTEX position,RECT rect, VERTEX center, char** pattern , bool tree)
{
	VERTEX difference;
	VERTEX currentPosition;

	for (int i = rect.y;i < rect.height;i++)
	{
		for (int j = rect.x;j < rect.width;j++)
		{
			difference.x = j - center.x;
			difference.y = i - center.y;

			currentPosition.x = position.x + difference.x;
			currentPosition.y = position.y + difference.y;

			if (pattern[i][j] != '\t')
			{
				frame[currentPosition.y][currentPosition.x] = pattern[i][j];
				
				if (tree)
				{
					goodForGlobes[indexGood] = currentPosition;
					indexGood++;
				}
			}
		}
	}
}

VERTEX DrawStand(VERTEX position,int size)
{
	char** pattern;
	VERTEX center;
	RECT rect;

	rect.x = 0;
	rect.y = 0;

	rect.width = 5 + (size +2 )*2;
	rect.height = size;

	center.x = rect.width / 2;
	center.y = rect.height - 1;

	pattern = new char*[size];

	for (int i = rect.y;i < size;i++)
	{
		pattern[i] = new char[rect.width];
		memset(pattern[i], '\t', sizeof(char) * rect.width);
	}

	int l = 3;
	int dif = size/2;

	bool cond = false;

	for (int i = rect.y;i < rect.height;i++)
	{
		for (int j = rect.x;j < rect.width;j++)
		{
			cond = false;
			if (i > rect.x)
			{
				if (j == center.x - dif)
				{
					pattern[i][j] = '/';
					cond = true;
				}
				if (j == center.x + dif)
				{
					pattern[i][j] = '\\';
					cond = true;
				}
			}

			if (!cond)
			{
				if (i == rect.y)
				{
					if (j > center.x - dif && j < center.x + dif)
					{
						pattern[i][j] = '_';
					}
				}
				else
				{
					if (j > center.x - dif && j < center.x + dif)
					{
						if (i == rect.height - 1)
						{
							pattern[i][j] = '_';
						}
						else
						{
							pattern[i][j] = ' ';
						}
					}
				}
			}
		}
		dif++;
	}

	DrawPattern(position,rect, center, pattern,false);

	position.y -= rect.height;

	return position;
}

VERTEX DrawTrunk(VERTEX position,int size)
{
	char** pattern;
	VERTEX center;
	RECT rect;

	rect.x = 0;
	rect.y = 0;
	rect.width = size / 2;
	rect.height = size;

	pattern = new char*[size];

	for (int i = rect.y;i < size;i++)
	{
		pattern[i] = new char[rect.width];
		memset(pattern[i], '\t', sizeof(char) * rect.width);
	}

	for (int i = rect.x;i < rect.height;i++)
	{
		for (int j = rect.y;j < rect.width;j++)
		{
			if (j == rect.y || j == rect.width - 1)
			{
				pattern[i][j] = '|';
			}
			else
			{
				pattern[i][j] = '@';
			}
		}
	}

	center.x = rect.width / 2;
	center.y = rect.height - 1;

	DrawPattern(position, rect, center, pattern,false);

	position.y -= rect.height;

	return position;
}

VERTEX DrawLayer(VERTEX position,  int height,int top,float rep)
{
	char** pattern;
	VERTEX center;
	RECT rect;

	float difference = top/2;

	rect.x = 0;
	rect.y = 0;
	rect.width = 2 + ((top+2)* height);
	rect.height = height;

	pattern = new char*[height];

	for (int i = rect.y;i < height;i++)
	{
		pattern[i] = new char[rect.width];
		memset(pattern[i], '\t', sizeof(char) * rect.width);
	}

	center.x = rect.width / 2;
	center.y = rect.height - 1;

	for (int i = rect.y;i < rect.height;i++)
	{
		for (int j = rect.x;j < rect.width;j++)
		{
			if (j == center.x - difference)
			{
				pattern[i][j] = '/';
			}
			else
			if (j == center.x + difference)
			{
				pattern[i][j] = '\\';
			}
			else
			{
				if (j > center.x - difference && j < center.x + difference)
				{
					if (i != rect.height - 1)
					{
						pattern[i][j] = ' ';
					}
					else
					{
						if (j % 2 == 0)
						{
							pattern[i][j] = '~';
						}
						else
						{
							pattern[i][j] = '_';
						}
					}
				}
			}
		}
		difference+=rep;
	}

	DrawPattern(position, rect, center, pattern,true);

	position.y -= rect.height;

	return position;
}

int GetPercent(int index)
{
	int res;
	if (index == 0)
	{
		index = 1;
	}

	return float((x / index));
}

int GetHeight(int index)
{
	int percent = 50/(x/1.5f);
	if (percent < 1)
	{
		percent = 1;
	}

	return percent;
}

int GetTop(int index)
{
	int percent;
	if (x < 50)
	{
		percent = 3 * (x - index) + (1 / 10 * x);
	}
	else
	{
		percent = (x - index) + (1 / 10 * x);
	}
	return percent;
}

float GetRep(int index)
{
	int percent =1 ;
	return percent;
}

void DrawGlobe(int type, VERTEX position)
{
	char** pattern;
	VERTEX center;
	RECT rect;

	switch (type)
	{
	case 0:

		pattern = new char*[3];
		for (int i = 0;i < 3;i++)
		{
			pattern[i] = new char[3];
		}

		pattern[0][0] = '@';
		pattern[0][1] = '-';
		pattern[0][2] = '@';

		pattern[1][0] = '|';
		pattern[1][1] = '#';
		pattern[1][2] = '|';

		pattern[2][0] = '@';
		pattern[2][1] = '-';
		pattern[2][2] = '@';

		center.x = 1;
		center.y = 2;

		rect.x = 0;
		rect.y = 0;
		rect.width = 3;
		rect.height = 3;

		break;

	case 1:
		pattern = new char*[1];
		pattern[0] = new char[1];

		pattern[0][0] = 'B';

		center.x = 0;
		center.y = 0;

		rect.x = 0;
		rect.y = 0;
		rect.width = 1;
		rect.height = 1;
		break;

	default:
		pattern = new char*[1];
		pattern[0] = new char[1];

		pattern[0][0] = '$';

		center.x = 0;
		center.y = 0;

		rect.x = 0;
		rect.y = 0;
		rect.width = 1;
		rect.height = 1;
		break;
	}


	DrawPattern(position, rect, center, pattern, false);
}

void DrawGlobes()
{
	for (int i = 0;i < 30;i++)
	{
		DrawGlobe(rand()%3, goodForGlobes[rand() % indexGood]);
	}
}

VERTEX DrawStar(VERTEX position,int radius)
{
	char** pattern;
	VERTEX center;
	RECT rect;

	rect.x = 0;
	rect.y = 0;
	rect.width = radius * 2;
	rect.height = radius * 2;

	center.x = rect.width / 2;
	center.y = rect.height / 2;

	pattern = new char*[rect.height];
	
	for (int i = rect.x;i < rect.height;i++)
	{
		pattern[i] = new char[rect.width];

		memset(pattern[i], '\t', sizeof(char) * rect.width);
	}

	for (int i = 0;i < rect.height;i++)
	{
		pattern[i][rect.width / 2] = '#';
	}

	for (int i = 0;i < rect.height;i)
	{
		for (int j = 0;j < rect.width;j++)
		{
			pattern[i][j] = '#';
			i++;
		}
	}

	for (int i = 0;i < rect.height;i)
	{
		for (int j = rect.width-1;j >= 0;j--)
		{
			pattern[i][j] = '#';
			i++;
		}
	}

	for (int i = 0;i < rect.width;i++)
	{
		pattern[rect.height/2][i] = '#';
	}
	DrawPattern(position, rect, center, pattern, false);

	return position;
}

void DrawTree(VERTEX position)
{
	position = DrawStand(position, GetTop(0)/4);
	position = DrawTrunk(position, GetTop(0)/3);

	for (int i = 0;i < x;i++)
	{
		position = DrawLayer(position,GetHeight(i), GetTop(i), GetRep(i));
	}

	position = DrawStar(position,10);

	DrawGlobes();
}

void main(int argc,char* argv[]) 
{
	ParseArguments(argc, argv);
	CreateOutputStream();
	InitFrame(WIDTH, HEIGHT);
	DrawTree(CalcTreePosition(WIDTH, HEIGHT));
	WriteOutputFrame(WIDTH, HEIGHT);

	return;
}