Cod Sursa: Main.cpp 
Compliator:Microsoft Visual Studio 2015

Pentru a afisa imaginea in format ASCII programul creeaza o matrice de caractere de dimensiunea 100x200 ( asa cum este definit in partea de sus a codului sursa ).
Aceasta matrice este incrementata cu valoarea '*' , urmand ca apoi pamantul pe care sta copacul sa fie incrementat cu valoarea '`'.
Apoi este inceputa desenarea bradului. Se incepe cu suportul, apoi este desenata tulpina acestuia iar in final layerele care au marimi diferite in functie de numarul acestora.
Toate formele sunt desenate prin intermediul functiei DrawPattern. Aceasta functie are ca paramterii pozitia obiectului, dreptunghiul in care este incadrata forma, centrul acesteia ( indicei ai matricei ) si matricea de caractere care reprezinta forma pe care sa o desneze;
Formele standului,ale tulpinei si ale layerelor sunt generate in timpul executiei programului, pe cand formele globurilor sunt definite in codul sursa.

Programul primeste ca parametrii numarul x ( numarul de layere ) si sirul de caractere path ( calea in care va fi salvat fisierul output ) . Aceste date sunt procesat de catre program la inceputul executiei acstuia, acesti parametrii fiind transmisi ca argumente din consola.